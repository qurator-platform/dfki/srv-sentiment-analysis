from python:3

COPY sentiment_analyser.py .
COPY flask-api.py .
COPY model model/

RUN pip install pandas
RUN pip install numpy
RUN pip install sklearn
RUN pip install flask

EXPOSE 8080
ENTRYPOINT FLASK_APP=flask-api.py flask run --host=0.0.0.0 --port=8080


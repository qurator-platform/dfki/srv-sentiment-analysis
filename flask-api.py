from flask import Flask
from flask import request

# creates a Flask application, named app
app = Flask(__name__)

import sentiment_analyser

@app.route("/", methods=['GET'])
def display_bot_message():
    user_input = request.args.get('msg')
    print(user_input)
    if user_input is not None:
        return sentiment_analyser.response(user_input)
    return sentiment_analyser.welcome_msg()

# run the application
if __name__ == "__main__":
    app.run(debug=True)
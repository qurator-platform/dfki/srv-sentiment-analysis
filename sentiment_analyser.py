# Text Classification (Movie Reviews)
# ----------------------------------------------------------------
# authors: Ela Elsholz
# date: August 2019
# description:
#   Sentiment analysis based on data set of movie reviews, bag-of-words as features
#   classes: 0-4 (very low - very high)
#   see kaggle competition: https://www.kaggle.com/ynouri/rotten-tomatoes-sentiment-analysis/data
# ----------------------------------------------------------------

import pandas as pd
import numpy as np
import pickle
from sklearn.svm import LinearSVC
from sklearn.feature_extraction.text import TfidfVectorizer

def train():
    '''
    Trains the sentiment analyser on labeled movie reviews of rotten tomatoes
    (train.tsv)
    '''
    # read training data
    trainData = pd.read_csv("train.tsv/train.tsv", header=0, delimiter='\t', quoting=3)

    # extract features
    vectorizer = TfidfVectorizer(ngram_range=(1,2))
    X = vectorizer.fit_transform(trainData["Phrase"])

    # train classifier with extracted features
    clf = LinearSVC()
    clf.fit(X, trainData["Sentiment"])

    # save model
    pickle.dump(vectorizer, open("model/vectorizer.pickle", "wb"))
    pickle.dump(clf, open("model/classifier.pickle", "wb"))

def response(request):
    '''
    Takes a request string;
    returns sentiment class.
    '''
    # load model
    vectorizer = pickle.load(open("model/vectorizer.pickle", "rb"))
    clf = pickle.load(open("model/classifier.pickle", "rb"))

    request_features = vectorizer.transform([request])
    prediction = clf.predict(request_features)
    return np.array2string(prediction)

def welcome_msg():
    '''
    Returns a welcome/info text about the service.
    '''
    msg = "This sentiment analyser returns classes from 0-4 (very low - very high). Test by typing any sentence."
    return msg


         


    



